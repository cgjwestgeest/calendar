package test;

import static org.junit.Assert.assertEquals;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import com.haynespro.assessment.dao.Event;
import com.haynespro.assessment.dao.EventDao;

public class EventDAOTest {
	
	private static final Logger LOG = LogManager.getLogger();

	@Test
	public void fullCrudTest() {	
		// try/catch to facilitate try-with-resources
		try {	 
		// create the eventDao 
	     EventDao eventDao = new EventDao();		
	    // choose a test event title that can be used for later filtering
	     String eventTitle =  "test@56859992";
		// create a test event
		 Event event = new Event();
		 event.setTitle(eventTitle);
		 event.setIsOption(1);
		 event.setIsPublic(0);
		 event.setType("show");
		 event.setStart(Timestamp.valueOf("2018-06-06 05:00:00.0"));
		 event.setEnd(Timestamp.valueOf("2018-06-06 06:00:00.0"));
		 event.setRemark("TestRemark"); 		 	 
		 // get the original table size
		 int originalDbSize =  eventDao.getEvents(getConnection()).size();		 
		 // create a new event
		 eventDao.saveEvent(getConnection(), event);
		 // get the entire content of the database table
		 List<Event> eventList = eventDao.getEvents(getConnection());
		 // assert that there is now an extra record in the DB
		 assertEquals(originalDbSize+1, eventList.size());
 		 // Get the newly created event from the event list
		 Event createdEvent = eventList.stream().filter(e -> e.getTitle().equals(eventTitle)).findFirst().orElse(null);	 
		 // get the Id for the newly created event
		 int EventId = createdEvent.getId();	 
		 // assert that we can retrieve the event with the eventId
		 assertEquals(eventTitle, eventDao.getEvent(getConnection(), EventId).getTitle());	 
		 // change the created event
		 createdEvent.setRemark("New Remark");
		 // update the created event
		 eventDao.saveEvent(getConnection(), createdEvent);
		 // assert that the event has been updated
		 assertEquals("New Remark", eventDao.getEvent(getConnection(), EventId).getRemark());
		 // delete the new event
		 eventDao.deleteEvent(getConnection(), EventId);
		 // assert that the original table size has been restored
		 assertEquals(originalDbSize, eventDao.getEvents(getConnection()).size());
		}
		catch (Exception e) {
			LOG.error(e);
		}
	}	
	/**
	 * Crude method to get a database connection. Needed for CRUD testing
	 * @return a connection
	 */
	private Connection getConnection() throws SQLException {
		 Connection conn = null;
		    Properties connectionProps = new Properties();
		    connectionProps.put("user", "test");
		    connectionProps.put("password", "test");
	        conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?serverTimezone=UTC", connectionProps);
		    System.out.println("Connected to database");
		    conn.setAutoCommit(false);
		return conn;
	}	
}

