package com.haynespro.assessment.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class EventDao {

	private static final Logger LOG = LogManager.getLogger();
	
	public static Map<String, String> NORMAL_COLORS = new HashMap<>();
	static {
		NORMAL_COLORS.put("show", "#c4a000");
		NORMAL_COLORS.put("event", "#ce5c00");
		NORMAL_COLORS.put("group_event", "#4e9a06");
		NORMAL_COLORS.put("classes", "#204a87");
		NORMAL_COLORS.put("workshop", "#5c3566");
		NORMAL_COLORS.put("private", "#a40000");
	}
	public static Map<String, String> OPTIONAL_COLORS = new HashMap<>();
	static {
		OPTIONAL_COLORS.put("show", "#fcf08d");
		OPTIONAL_COLORS.put("event", "#fcd090");
		OPTIONAL_COLORS.put("group_event", "#ade279");
		OPTIONAL_COLORS.put("classes", "#cbe0ff");
		OPTIONAL_COLORS.put("workshop", "#efceec");
		OPTIONAL_COLORS.put("private", "#fdbebe");
	}

	private String getColor(Event e) {
		return e.getIsOption() == 0 ? NORMAL_COLORS.get(e.getType()) : OPTIONAL_COLORS.get(e.getType());
	}

	public List<Event> getEvents(Connection connection) {

		List<Event> events = new ArrayList<>();

		try {

			String sql = "SELECT * FROM events";
			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				Event e = new Event();
				e.setId(rs.getInt("id"));
				e.setTitle(rs.getString("title"));
				e.setRemark(rs.getString("remark"));
				e.setStart(rs.getTimestamp("start"));
				e.setEnd(rs.getTimestamp("end"));
				e.setIsOption(rs.getInt("optional"));
				e.setIsPublic(rs.getInt("public"));
				e.setType(rs.getString("type"));
				e.setColor(getColor(e));
				events.add(e);
			}

		} catch (Exception e) {
			LOG.error(e);
		} finally {
			closeConnection(connection);
		}

		return events;
	}

	public void saveEvent(Connection connection, Event event) {
		
		if (event.getId() == 0) {
			insert(connection, event);
		} else {
			update(connection, event);
		}
		
	}
	
	private void insert(Connection connection, Event event) {
		
		String sql = "INSERT INTO events(title,start,end,public,type,remark,optional) VALUES(?,?,?,?,?,?,?)";
			
		try {	
		
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, event.getTitle());
			statement.setTimestamp(2, event.getStart());
			statement.setTimestamp(3, event.getEnd());		
			statement.setInt(4, event.getIsPublic());
			statement.setString(5,  event.getType());
			statement.setString(6, event.getRemark());
			statement.setInt(7, event.getIsOption());
						
			statement.executeUpdate();	
		
		} catch (SQLException e) {
			LOG.error(e);
		} finally {
			closeConnection(connection);
		}
	}

	private void update(Connection connection, Event event) {
		
		String sql = "UPDATE events SET title=?, start=?, end=?, public=?, type=?, remark=? ,optional=? WHERE id = ?";
		
		try {	
		
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, event.getTitle());
			statement.setTimestamp(2, event.getStart());
			statement.setTimestamp(3, event.getEnd());		
			statement.setInt(4, event.getIsPublic());
			statement.setString(5,  event.getType());
			statement.setString(6, event.getRemark());
			statement.setInt(7, event.getIsOption());
			statement.setInt(8,  event.getId());
			
			statement.executeUpdate();	
		
		} catch (SQLException e) {
			LOG.error(e);
		} finally {
			closeConnection(connection);
		}
		
	}

	public Event getEvent(Connection connection, int id) {
		
		Event event = new Event();
		
		try {
			String sql = "SELECT * FROM events WHERE id = ?";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1,  id);
			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
	
				event.setId(rs.getInt("id"));
				event.setTitle(rs.getString("title"));
				event.setRemark(rs.getString("remark"));
				event.setStart(rs.getTimestamp("start"));
				event.setEnd(rs.getTimestamp("end"));
				event.setIsOption(rs.getInt("optional"));
				event.setIsPublic(rs.getInt("public"));
				event.setType(rs.getString("type"));
				event.setColor(getColor(event));
			}

		} catch (Exception e) {
			LOG.error(e);
		} finally {
			closeConnection(connection);
		}
		
		return event;	
	}

	public void updateEvent(Connection connection, Event event) {
		
		String sql = "UPDATE events SET start=?, end=? WHERE id=?";
		
		try {	
		
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setTimestamp(1, event.getStart());
			statement.setTimestamp(2, event.getEnd());		
			statement.setInt(3,  event.getId());
			
			statement.executeUpdate();	
		
		} catch (SQLException e) {
			LOG.error(e);
		} finally {
			closeConnection(connection);		
		}
	}

	public void deleteEvent(Connection connection, int id) {

		String sql = "DELETE FROM events WHERE id=?";

		try {
			
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1,  id);
			
			statement.executeUpdate();
			
		} catch (SQLException e) {
			LOG.error(e);
		} finally {
			closeConnection(connection);
		}
	}
	
	private void closeConnection(Connection connection) {
		try {
			connection.close();
		} catch (SQLException e) {
			LOG.error("Failed to close database connection. Cause: "+e.getMessage());
		}
	}
}
