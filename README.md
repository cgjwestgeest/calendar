**README**
- 

**First-glance assesment**
1. DAO methods only partially implemented
2. Unittests not implemented
3. Exception handling only partially implemented in controllers

**Assumptions**
1. Little to no changes to front-end required. Mainly focus on back-end
2. Stick to the dependecies as provided (no Lombok or other helper libs)

**Challenges**
1. Regular way of using prepared statements gives a syntax error. Reason unknown. Triple-checked, but cannot find the error. Syntax error is also thrown when inserting via mysql shell. <-- changed the name of the table from option to optional, to ensure the error is not due to use of a reserved word. This resolved the problem, though I'm not certain the root cause is the table name.

**Remarks**
1. On the EventDaoTest --> I realize it's bad practice to do a CRUD unittest against your main database. I'd prefer to run the unittests against a temporary (h2) db, but that'd be impossible without adding dependencies. as I don't want to skip unittests altogether, this seemed the best way.
2. It seems like the controllers are lacking proper request validation on POST/PUT, but this may be because the FE and BE are intended to be tightly coupled, and input validation should be handled at the FE. 
3. There is currently no real error-handling between the controllers and the FE. However, implementing this properly would also require additions to the FE to properly handle error responses (error pages etc).
